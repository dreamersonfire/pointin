<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Place;
use DB;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($city = null)
    {
        if($city == null){
            return null;
        }
        $locations = Place::get()->where('city', $city);
        return View('category', array('locations' => $locations));
    }


    public function getContacts(Request $request)
    {
        $rawContacts = $request->input('data');
        $totalContacts = count($rawContacts);
        for($i = 0 ; i < $totalContacts; $i++){
            
        }

    }


    public function registerNumber(Request $request)
    {
        try{
           $phone = $request->input('phone');
           $number = $request->input('token');        
            
            DB::table('register')->insert(array("phone" => $phone, "token" => $number));
            return "success";
        }
        catch(exception $ex){
            return "failure";
        }

    }

    public function getPlacesAround($city = null, $type = null)
    {
        $city = $city;
        $placeType = $type;
        // if($city == null &&  $type == null){
        //     return DB::table('location')->
        // }
        $allPlaces = DB::table('location')->where('city', $city)->where('type', $placeType)->get();
        return $allPlaces; 
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}