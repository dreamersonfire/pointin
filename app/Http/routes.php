<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

#Route::get('/', function () {
    #return view('welcome');
#});
Route::get('/', function () {
    return view('index');
});
Route::get('category', function () {
    return view('category');
});
Route::get('features', function () {
    return view('features');
});



Route::get('places/{city?}', 'PlaceController@show');

Route::get('contacts', 'PlaceController@getContacts');

Route::get('placesAround/{city?}/{type?}', 'PlaceController@getPlacesAround');

Route::get('register', 'PlaceController@registerNumber');


