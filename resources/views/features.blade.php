@extends('layouts.master')
@section('title', 'Page Title')
@section('content')
<section class="band">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="cat-bg clearfix relative">
                    <h1 class="relative">
                        <span>Features</span>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
            <div class="panel f">
              <div class="panel-heading"><h3 class="panel-title"><span class="fa fa-floppy-o"></span>Save locations</h3></div>
                    <div class="panel-body">
                        <ul>
                            <li>Save current location</li>
                            <li>Search any location on the earth ans Save it</li>
                            <li>Maintain saved locations as a history.</li>
                            <li>Delete any of the saved locations.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel f">
                <div class="panel-heading"><h3 class="panel-title"><span class="fa fa-share"></span>Flexibility in sharing </h3></div>
                    <div class="panel-body">
                        <ul>
                            <li>Share location as a event.</li>
                            <li>Share location as a address card.</li>
                            <li>You can post any of the saved locations on facebook as a check in.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel f">
                    <div class="panel-heading"><h3 class="panel-title"><span class="fa fa-location-arrow"></span>Navigation to places</h3></div>
                    <div class="panel-body">
                        <ul>
                            <li>You can navigate people to your event easily.</li>
                            <li>2 You can provide address in advanced way.</li>
                            <li>3 Get navigation to all your saved places.</li>
                            <li>4 Get navigation to all recommended places around you.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel f">
                <div class="panel-heading"><h3 class="panel-title"><span class="fa fa-thumbs-up"></span>Recommendation</h3></div>
                    <div class="panel-body">
                        <ul>
                            <li>You can recommend your locations to others.</li>
                            <li>You can get recommendations of places around you.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel f">
                <div class="panel-heading"><h3 class="panel-title"><span class="fa fa-map-marker"></span>Request location</h3></div>
                    <div class="panel-body">
                        <ul>
                            <li>You can send your current location to your friend in fastest way ever.</li>
                            <li>You can request location to any of your friend easily.</li>
                        </ul>
                    </div>
                </div>
            </div>



        </div>
    </div>
</section>



@endsection