@extends('layouts.master')
@section('title', 'Page Title')
@section('content')
<section class="search-wrapper">
    <div class="container text-center">
        <!-- Large modal -->
        <button type="button" class="hidden btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>



            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>PointIn</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt <br>ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <form action="" method="POST" role="form" class="searchbar">
                <input tabindex="1" type="text" class="form-control" id="" placeholder="I am looking for...">
                <button tabindex="2" type="submit" class="btn btn-search">
                    <span class="glyphicon glyphicon-search"></span> Search
                </button>
            </form>
            <div class="search-cat">
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/hotel.png" alt="">Hotels</a>
                    </li>
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/cafe.png" alt="">Cafe</a>
                    </li>
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/food.png" alt="">Restaurants</a>
                    </li>
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/garden.png" alt="">Gardens</a>
                    </li>
                </ul>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/hillstation.png" alt="">Hillstations</a>
                    </li>
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/mall.png" alt="">Shopping Malls</a>
                    </li>
                    <li role="presentation">
                        <a href="#"><img height="24" src="/img/icons/petrolpump.png" alt="">Fuel Stations</a>
                    </li>
                    <li role="presentation">
                        <a href="/places/pune"><img height="24" src="/img/icons/other.png" alt="">All</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</section>
@endsection