@extends('layouts.master')
@section('title', 'Page Title')
@section('content')
<section class="band">
    <div class="cat-bg clearfix relative">
        <h1 class="relative">
            <span>Pune</span>
        </h1>
        <div class="cat-wrapper">
            <button class="close" type="button">&times;</button>
            <ul class="list-inline cat-list relative">
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
                <li><a class="tag" href="#">Cat</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="filter-panel" class="filter-panel hidden">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-inline" role="form">
                                <div class="form-group">
                                    <input type="text" placeholder="Filter" class="form-control">
                                </div><!-- form group [search] -->
                                <div class="form-group">
                                    <label class="filter-col" style="margin-right:0;" for="pref-orderby">Order by:</label>
                                    <select id="pref-orderby" class="form-control">
                                        <option>Descendent</option>
                                    </select>                                
                                </div> <!-- form group [order by] --> 
                                <div class="form-group">    
                                    <div class="checkbox" style="margin-left:10px; margin-right:10px;">
                                        <label><input type="checkbox"> Remember parameters</label>
                                    </div>
                                    <button type="submit" class="btn btn-default filter-col">
                                        <span class="glyphicon glyphicon-record"></span> Save Settings
                                    </button>  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row masonry-container">
            @foreach ($locations as $location)
            <div class="col-md-4 item">
                <div class="open-detail-panel panel panel-default">
                    <div class="panel-body">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-share btn-brand dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-share"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h3 class="capitalize"><span class="type-icon"><img src="/img/icons/{{$location -> type}}.png" alt="" height="16"></span>{{$location -> desc}}</h3>
                        <div class="meta clearfix">
                            <p><span>Type:</span> <strong class="capitalize">{{$location -> type}}</strong></p>
                            <span>Rating:</span>
                            {{$location -> rating}}
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


<div class="detail-panel">
    <button class="close" type="">&times;</button>
    <div class="panel-body">
        <h2 class="title">askj d sakldjsakld jksl</h2>
        <ul class="list-inline list-unstyled type">
            <li>Type: <strong>Food</strong></li>
            <li>
                <i class="fa fa-star" aria-hidden="true"></i> 
                <i class="fa fa-star" aria-hidden="true"></i> 
                <i class="fa fa-star" aria-hidden="true"></i></li>
            </ul>
            <div id="shareIcons"></div>
        </div>
    </div>
    <div class="details-overlay">

    </div>



    @endsection