jQuery(document).ready(function($) {
	var $container = $('.masonry-container');
	$container.imagesLoaded( function () {
	  $container.masonry({
	    columnWidth: '.item',
	    itemSelector: '.item'
	  });   
	});

	$( document ).ajaxStart(function() {
	  $( "#loading" ).fadeIn('fast');
	});
	$( document ).ajaxStop(function() {
	  $( "#loading" ).fadeOut('fast');
	});
	$(".dropdown").hover(
		function() {
			$('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
			$(this).toggleClass('open');
		},
		function() {
			$('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
			$(this).toggleClass('open');
		});
	$('.show-cat-list').on('click', function() {
		$('.cat-wrapper').fadeIn('fast');
	});
	$('.cat-wrapper .close').on('click', function() {
		$('.cat-wrapper').fadeOut('fast');
	});
	openDetailsPanel();
	closeDetailsPanel();
});
function openDetailsPanel() {
	$('.open-detail-panel').on('click', function(){
		$('.detail-panel').addClass('pulled');
		$('.details-overlay').show();
		closePaneOnEscape();


		$("#shareIcons").jsSocials({
		showLabel: false,
		showCount: false,
		shareIn: "popup",
		shares: ["email", "facebook", "twitter", "pinterest", "stumbleupon"]
	});

	});
}
function closeDetailsPanel() {
	$('.detail-panel .close').on('click', function(){
		$('.detail-panel .panel-body').html('');
		$('.detail-panel').removeClass('pulled');
		$('.details-overlay').hide();
	});
}
function closePaneOnEscape() {
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.detail-panel .panel-body').html('');
			$('.detail-panel').removeClass('pulled');
			$('.details-overlay').fadeOut();
		}
	});
}

jQuery(window).resize(function(event) {
	var $container = $('.masonry-container');
	$container.imagesLoaded( function () {
	  $container.masonry({
	    columnWidth: '.item',
	    itemSelector: '.item'
	  });   
	});
});